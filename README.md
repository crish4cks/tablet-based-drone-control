# Tablet-based drone control
This repository contains all the material concerning a master thesis project which purpose was to
realize a software to control drone movements through a graphical interface.
The whole software is composed by a client (Scribbling Client) and a server (Scribbling Server). 
The client runs on a portable device controlled by the user, the server runs on the drone onboard 
computer.

### Contents
* __/doc__: contains all the project documentation (see msc-thesis.pdf)
* __/media__: here are stored some videos showing flight simulations
* __/scripts__: some useful Bash scripts (e.g. for automatically creating a WiFi access point)
* __/simulations__: some screenshots of old simulations and some notes [mostly deprecated]
* __/src__: contains the source code of Scribbling Client and Scribbling Server with also a copy of the
Onboard SDK (version 3.9) used during the development phase.

Some important information can also be found by looking at the repository issues section.

### Author
Cristiano Urban (https://crish4cks.net)

e-mails:

*  cristiano[dot]urban[dot]slack[at]gmail[dot]com
*  urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
