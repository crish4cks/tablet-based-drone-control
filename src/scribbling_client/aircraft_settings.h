/*
 * aircraft_settings.h
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


#ifndef AIRCRAFT_SETTINGS_H
#define AIRCRAFT_SETTINGS_H 

#include <QGroupBox>
#include <QLabel>
#include <QDialog>
#include <QDialogButtonBox>
#include <QRadioButton>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QVBoxLayout>


class AircraftSettings : public QDialog {
    Q_OBJECT
    
public:
    AircraftSettings();
    bool getFlightMode() const;
    float getInitialHeight() const { return initialHeightSpinBox->value(); }    
    void setFlightMode(const bool &flightMode);
    void setInitialHeight(const float &initialHeight);
    
private:
    void createFlightModeSettings();
    void createInitialHeightSettings();    
    
    QGroupBox *flightModeSettingsGroup;
    QGroupBox *initialHeightSettingsGroup;
    QDialogButtonBox *confirmButtonBox; 
    QRadioButton *horiFlightModeRadioButton;
    QRadioButton *vertFlightModeRadioButton;
    QDoubleSpinBox *initialHeightSpinBox;
    QLabel *initialHeightLabel;
};

#endif