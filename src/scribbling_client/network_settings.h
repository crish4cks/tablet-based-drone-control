/*
 * network_settings.h
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


#ifndef NETWORK_SETTINGS_H
#define NETWORK_SETTINGS_H

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>


class NetworkSettings : public QDialog {
    Q_OBJECT
    
public:
    NetworkSettings();
    QString getOnboardIPAddress() const { return ipLineEdit->text(); }
    int getOnboardTcpPort() const { return portSpinBox->value(); }
    int getScaleFactor() const { return scaleFactorSpinBox->value(); }
    int getSamplingFactor() const { return samplingFactorSpinBox->value(); }
    void setOnboardIPAddress(QString ip_addr);
    void setOnboardTcpPort(int tcp_port);
    void setScaleFactor(int scale_factor);
    void setSamplingFactor(int sampling_factor);
    
private:
    void createOnboardSettings();
    void createClientSettings();
    
    QGroupBox *onboardSettingsGroup;
    QGroupBox *clientSettingsGroup;
    QLabel *ipLabel;
    QLabel *portLabel;
    QLabel *scaleFactorLabel;
    QLabel *samplingFactorLabel;
    QLineEdit *ipLineEdit;
    QSpinBox *portSpinBox;
    QSpinBox *scaleFactorSpinBox;
    QSpinBox *samplingFactorSpinBox;
    QDialogButtonBox *confirmButtonBox;
};

#endif