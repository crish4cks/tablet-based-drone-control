/*
 * network_settings.cpp
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


#include "network_settings.h"


NetworkSettings::NetworkSettings() {
    createOnboardSettings();
    createClientSettings();
    
    confirmButtonBox = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
    connect(confirmButtonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(confirmButtonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    
    QVBoxLayout *vlayout = new QVBoxLayout;
    
    vlayout->addWidget(onboardSettingsGroup);
    vlayout->addWidget(clientSettingsGroup);
    vlayout->addWidget(confirmButtonBox);
    setLayout(vlayout);
    setWindowTitle(tr("Network Settings"));
}

void NetworkSettings::createOnboardSettings() {  
    ipLabel = new QLabel(tr("IP address:"));
    portLabel = new QLabel(tr("TCP port:"));
    scaleFactorLabel = new QLabel(tr("Scale factor [px/m]:"));
    
    ipLineEdit = new QLineEdit;
    ipLineEdit->setInputMask(QString("000.000.000.000"));

    portSpinBox = new QSpinBox;
    portSpinBox->setRange(1, 65535);
    portSpinBox->setSingleStep(1);
    
    scaleFactorSpinBox = new QSpinBox;
    scaleFactorSpinBox->setMinimum(1);
    scaleFactorSpinBox->setMaximum(1000);
    scaleFactorSpinBox->setSingleStep(1);
    
    onboardSettingsGroup = new QGroupBox(tr("Scribbling Server"));
    QFormLayout *layout = new QFormLayout;
    layout->addRow(ipLabel, ipLineEdit);
    layout->addRow(portLabel, portSpinBox);
    layout->addRow(scaleFactorLabel, scaleFactorSpinBox);        
    onboardSettingsGroup->setLayout(layout);
}

void NetworkSettings::createClientSettings() {  
    samplingFactorLabel = new QLabel(tr("Cursor sampling factor:"));
  
    samplingFactorSpinBox = new QSpinBox;
    samplingFactorSpinBox->setRange(1, 10);
    samplingFactorSpinBox->setSingleStep(1);
        
    clientSettingsGroup = new QGroupBox(tr("Scribbling Client"));
    QFormLayout *layout = new QFormLayout;
    layout->addRow(samplingFactorLabel, samplingFactorSpinBox);
    clientSettingsGroup->setLayout(layout);
}

void NetworkSettings::setOnboardIPAddress(QString ip_addr) {
    ipLineEdit->setText(ip_addr);
}
    
void NetworkSettings::setOnboardTcpPort(int tcp_port) {
    portSpinBox->setValue(tcp_port);  
}    
    
void NetworkSettings::setScaleFactor(int scale_factor) {
    scaleFactorSpinBox->setValue(scale_factor);
}

void NetworkSettings::setSamplingFactor(int sampling_factor) {
    samplingFactorSpinBox->setValue(sampling_factor);
}