/*
 * mainwindow.cpp
 * 
 * This is a modified version of the MainWindow class
 * (https://doc.qt.io/qt-5/qtwidgets-widgets-scribble-example.html)
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwindow.h"
#include "scribblearea.h"
#include "network_settings.h"
#include "aircraft_settings.h"

#include <QApplication>
#include <QColorDialog>
#include <QFileDialog>
#include <QImageWriter>
#include <QInputDialog>
#include <QMenuBar>
#include <QGridLayout>
#include <QMessageBox>
#include <QCloseEvent>

#include <QApplication>
#include <QSettings>


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    QWidget *mainWidget = new QWidget(this);
    QWidget *statusWidget = new QWidget(mainWidget);
    
    setMouseTracking(true);
    
    connLabel = new QLabel(statusWidget);
    connLabel->setAlignment(Qt::AlignRight);
    
    onboardStatusLed = new QRadioButton(statusWidget);
    onboardStatusLed->setStyleSheet("QRadioButton::indicator {width: 15px; height: 15px; border-radius: 9px;} \
                                     QRadioButton::indicator:unchecked { background-color: #B5B2B1; border: 2px solid gray;} \
                                     QRadioButton::indicator:checked { background-color: #B5B2B1; border: 2px solid gray;}");
    
    flightModeLabel = new QLabel(statusWidget);
    flightModeLabel->setAlignment(Qt::AlignCenter);
    flightModeLabel->setStyleSheet("font-size: 10pt; font-weight: bold;");
        
    scribbleArea = new ScribbleArea(this);
    
    QGridLayout *glayout = new QGridLayout(mainWidget);
    glayout->addWidget(onboardStatusLed, 0, 0, 1, 1);
    glayout->addWidget(connLabel, 0, 2, 1, 1);
    glayout->addWidget(flightModeLabel, 0, 1, 1, 1);
    glayout->addWidget(statusWidget, 0, 2, 1, 1);
    glayout->addWidget(scribbleArea, 1, 0, 1, 3);
    
    statusWidget->setMaximumHeight(15);
    statusWidget->setMaximumWidth(800);
    
    mainWidget->setLayout(glayout);

    setCentralWidget(mainWidget);

    createActions();
    createMenus();
    
    // Trigger the updateConnState private SLOT every 0.5 seconds to retrieve the connection state
    updateConnStateTimer = new QTimer(this);
    connect(updateConnStateTimer, &QTimer::timeout, this, &MainWindow::updateConnState);    
    updateConnStateTimer->start(500);
    
    // Receive signal to update the onboard computer status led on the GUI
    connect(scribbleArea, SIGNAL(onboardStatusChanged(int)), this, SLOT(updateOnboardStatusLed(int)));
    
    // Receive signal to update aircraft actions menu
    connect(scribbleArea, SIGNAL(onboardStatusChanged(int)), this, SLOT(enableAircraftActions(int)));

    // Receive signal to enable/disable aircraft settings
    connect(scribbleArea, SIGNAL(onboardStatusChanged(int)), this, SLOT(enableAircraftSettings(int)));

    // Timer for the blinking led
    blinkOnErrorTimer = new QTimer(this);
    connect(blinkOnErrorTimer, &QTimer::timeout, this, &MainWindow::blinkOnError);
    
    connectAct->setEnabled(false);
    disconnectAct->setEnabled(true);
    
    takeOffAct->setEnabled(false);
    landingAct->setEnabled(false);
    
    netSettings = new NetworkSettings();
    aircftSettings = new AircraftSettings();
    
    // Load previously saved settings
    loadSettings();    
    
    setWindowTitle(tr("Scribbling Client"));
    resize(800, 600);
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if (maybeSave()) {
        event->accept();
	scribbleArea->stopTcpConn();
    }
    else
        event->ignore();
}

void MainWindow::save()
{
    QAction *action = qobject_cast<QAction *>(sender());
    QByteArray fileFormat = action->data().toByteArray();
    saveFile(fileFormat);
}

void MainWindow::networkSettings() {
    if(scribbleArea->getConnState() == QTcpSocket::ConnectedState)
        netSettings->setEnabled(false);
    else netSettings->setEnabled(true);

    if(netSettings->exec() == QDialog::Accepted) {
        ip_addr = netSettings->getOnboardIPAddress();
	tcp_port = netSettings->getOnboardTcpPort();
	
	scale_factor = netSettings->getScaleFactor();
	scribbleArea->setScaleFactor(scale_factor);
	
	sampling_factor = netSettings->getSamplingFactor();
	scribbleArea->setCursorSamplingFactor(sampling_factor);
	
	saveSettings();
    }
    else loadSettings();
}

void MainWindow::aircraftSettings() { 
    if(aircftSettings->exec() == QDialog::Accepted) {
        flightMode = aircftSettings->getFlightMode();
	initialHeight = aircftSettings->getInitialHeight();
        scribbleArea->setFlightMode(flightMode);
	scribbleArea->setInitialHeight(initialHeight);
        updateFlightModeLabel();
        saveSettings();
    }
    else loadSettings();
}

void MainWindow::startConn() {  
    scribbleArea->startTcpConn(ip_addr, tcp_port);
}

void MainWindow::stopConn() {
    scribbleArea->stopTcpConn();
}

void MainWindow::updateConnState() {
    if(scribbleArea->getConnState() == QTcpSocket::ConnectedState) {
	connLabel->setStyleSheet("font-size: 9pt; font-weight: bold; color: green;");
	connLabel->setText("CONNECTED");
	if(connectAct->isEnabled() && !disconnectAct->isEnabled()) {
	    connectAct->setEnabled(false);
	    disconnectAct->setEnabled(true);
	}
    }
    else { 
        connLabel->setStyleSheet("font-size: 9pt; font-weight: bold; color: red;");
	connLabel->setText("DISCONNECTED");
	if(!connectAct->isEnabled() && disconnectAct->isEnabled()) {
	    connectAct->setEnabled(true);
	    disconnectAct->setEnabled(false);
	}
	takeOffAct->setEnabled(false);
        landingAct->setEnabled(false);
    }
}

void MainWindow::updateOnboardStatusLed(int statusCode) {
    if(statusCode == 0 || statusCode == 1) {
        onboardStatusLed->setStyleSheet("QRadioButton::indicator {width: 15px; height: 15px; border-radius: 9px;} \
                                         QRadioButton::indicator:unchecked { background-color: lime; border: 2px solid gray;} \
                                         QRadioButton::indicator:checked { background-color: lime; border: 2px solid gray;}");   
    }  
    else if(statusCode == 2) {
        onboardStatusLed->setStyleSheet("QRadioButton::indicator {width: 15px; height: 15px; border-radius: 9px;} \
                                         QRadioButton::indicator:unchecked { background-color: #BD0000; border: 2px solid gray;} \
                                         QRadioButton::indicator:checked { background-color: #BD0000; border: 2px solid gray;}");       
    } 
    else { // statusCode == 3 (ERROR)
        onboardStatusLed->setStyleSheet("QRadioButton::indicator {width: 15px; height: 15px; border-radius: 9px;} \
                                         QRadioButton::indicator:unchecked { background-color: #B5B2B1; border: 2px solid gray;} \
                                         QRadioButton::indicator:checked { background-color: #B5B2B1; border: 2px solid gray;}");
        blinkOnErrorTimer->start(1000);
    }
}

void MainWindow::updateFlightModeLabel() {
    if(flightMode)
        flightModeLabel->setText("[y-z]");  
    else
        flightModeLabel->setText("[x-y]");  
}

void MainWindow::enableAircraftActions(int statusCode) {
    if(statusCode == 0) {         // INIT
        takeOffAct->setEnabled(true);
        landingAct->setEnabled(false);
    }
    else if(statusCode == 1) {    // READY
        takeOffAct->setEnabled(false);
        landingAct->setEnabled(true); 
    }
    else if(statusCode == 2) {    // BUSY
         takeOffAct->setEnabled(false);
         landingAct->setEnabled(false);      
    }
    else {                        // ERROR
        // do nothing...
    }
}

void MainWindow::enableAircraftSettings(int statusCode) {
    if(statusCode == 0)    // INIT
        aircftSettings->setEnabled(true);
    else aircftSettings->setEnabled(false);
}

void MainWindow::blinkOnError() {
    if(blinkFlag == true)
        blinkFlag = false;
    else blinkFlag = true;
    
    if(blinkFlag == true) {
        onboardStatusLed->setStyleSheet("QRadioButton::indicator {width: 15px; height: 15px; border-radius: 9px;} \
                                         QRadioButton::indicator:unchecked { background-color: #FFEC15; border: 2px solid gray;} \
                                         QRadioButton::indicator:checked { background-color: #FFEC15; border: 2px solid gray;}");        
    }
    else {
        onboardStatusLed->setStyleSheet("QRadioButton::indicator {width: 15px; height: 15px; border-radius: 9px;} \
                                         QRadioButton::indicator:unchecked { background-color: #B5B2B1; border: 2px solid gray;} \
                                         QRadioButton::indicator:checked { background-color: #B5B2B1; border: 2px solid gray;}");            
    }
}

void MainWindow::penColor()
{
    QColor newColor = QColorDialog::getColor(scribbleArea->penColor());
    if (newColor.isValid())
        scribbleArea->setPenColor(newColor);
}

void MainWindow::penWidth() {
    bool ok;
    int newWidth = QInputDialog::getInt(this, tr("Scribbling Client"),
                                        tr("Select pen width:"),
                                        scribbleArea->penWidth(),
                                        1, 50, 1, &ok);
    if (ok)
        scribbleArea->setPenWidth(newWidth);
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About Scribbling Client"),
            tr("<p>The <b>Scribbling Client</b> allows to reproduce in the "
               "real world, through a drone, the actions made by the "
               "user on a drawing area.</p><p>This software has been "
               "successfully tested with the DJI M100 quadcopter.</p>"
	       "<p>The code has been written starting from the "
	       "<a href=https://github.com/qt/qtbase/tree/dev/examples/widgets/widgets/scribble>scribble example</a>.</p>"
	       "<br><p><i>Author: Cristiano Urban, </i><a href=https://crish4cks.net>https://crish4cks.net</a><p>"
               "<p><i>University of Udine<br>A.Y. 2019/2020</i><br>"
	       "<a href=https://www.uniud.it>https://www.uniud.it</a></p>"));
}

void MainWindow::createActions() {    
    const QList<QByteArray> imageFormats = QImageWriter::supportedImageFormats();
    for (const QByteArray &format : imageFormats) {
        QString text = tr("%1...").arg(QString::fromLatin1(format).toUpper());

        QAction *action = new QAction(text, this);
        action->setData(format);
        connect(action, &QAction::triggered, this, &MainWindow::save);
        saveAsActs.append(action);
    }

    printAct = new QAction(tr("&Print..."), this);
    connect(printAct, &QAction::triggered, scribbleArea, &ScribbleArea::print);

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    connect(exitAct, &QAction::triggered, this, &MainWindow::close);

    networkSettingsAct = new QAction(tr("&Network Settings..."), this);
    connect(networkSettingsAct, &QAction::triggered, this, &MainWindow::networkSettings);

    penColorAct = new QAction(tr("&Pen Color..."), this);
    connect(penColorAct, &QAction::triggered, this, &MainWindow::penColor);

    penWidthAct = new QAction(tr("Pen &Width..."), this);
    connect(penWidthAct, &QAction::triggered, this, &MainWindow::penWidth);

    clearScreenAct = new QAction(tr("&Clear Screen"), this);
    clearScreenAct->setShortcut(tr("Ctrl+L"));
    connect(clearScreenAct, &QAction::triggered,
            scribbleArea, &ScribbleArea::clearImage);
    
    connectAct = new QAction(tr("&Connect"), this);
    connect(connectAct, &QAction::triggered, this, &MainWindow::startConn);
    
    disconnectAct = new QAction(tr("&Disconnect"), this);
    connect(disconnectAct, &QAction::triggered, this, &MainWindow::stopConn);
    
    aircraftSettingsAct = new QAction(tr("&Settings..."), this);
    connect(aircraftSettingsAct, &QAction::triggered, this, &MainWindow::aircraftSettings);
    
    takeOffAct = new QAction(tr("&Take-off"), this);
    connect(takeOffAct, &QAction::triggered, scribbleArea, &ScribbleArea::sendTakeOffCmd);
    
    landingAct = new QAction(tr("&Landing"), this);
    connect(landingAct, &QAction::triggered, scribbleArea, &ScribbleArea::sendLandingCmd);
    
    aircraftActs.append(takeOffAct);
    aircraftActs.append(landingAct);
    
    aboutAct = new QAction(tr("&About"), this);
    connect(aboutAct, &QAction::triggered, this, &MainWindow::about);

    aboutQtAct = new QAction(tr("About &Qt"), this);
    connect(aboutQtAct, &QAction::triggered, qApp, &QApplication::aboutQt);
}

void MainWindow::createMenus() {
    saveAsMenu = new QMenu(tr("&Save As"), this);
    for (QAction *action : qAsConst(saveAsActs))
        saveAsMenu->addAction(action);
    
    takeOffandLandingMenu = new QMenu(tr("&Actions"), this);
    for (QAction *action : qAsConst(aircraftActs))
        takeOffandLandingMenu->addAction(action);
    
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addMenu(saveAsMenu);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    optionMenu = new QMenu(tr("&Options"), this);
    optionMenu->addAction(networkSettingsAct);
    optionMenu->addAction(penColorAct);
    optionMenu->addAction(penWidthAct);
    optionMenu->addSeparator();
    optionMenu->addAction(clearScreenAct);
    
    onboardMenu = new QMenu(tr("&Manifold"), this);
    onboardMenu->addAction(connectAct);
    onboardMenu->addAction(disconnectAct);
    
    aircraftMenu = new QMenu(tr("&Aircraft"), this);
    aircraftMenu->addAction(aircraftSettingsAct);
    aircraftMenu->addMenu(takeOffandLandingMenu);
    
    helpMenu = new QMenu(tr("&Help"), this);
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(optionMenu);
    menuBar()->addMenu(onboardMenu);
    menuBar()->addMenu(aircraftMenu);
    menuBar()->addMenu(helpMenu);
}

bool MainWindow::maybeSave() {
    if (scribbleArea->isModified()) {
       QMessageBox::StandardButton ret;
       ret = QMessageBox::warning(this, tr("Scribbling Client"),
                          tr("The image has been modified.\n"
                             "Do you want to save your changes?"),
                          QMessageBox::Save | QMessageBox::Discard
                          | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return saveFile("png");
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}

bool MainWindow::saveFile(const QByteArray &fileFormat)
{
    QString initialPath = QDir::currentPath() + "/untitled." + fileFormat;

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                               initialPath,
                               tr("%1 Files (*.%2);;All Files (*)")
                               .arg(QString::fromLatin1(fileFormat.toUpper()))
                               .arg(QString::fromLatin1(fileFormat)));
    if (fileName.isEmpty())
        return false;
    return scribbleArea->saveImage(fileName, fileFormat.constData());
}

// On Linux the settings will be saved in 
// $HOME/.config/uniud.it/scribbling_client.conf
void MainWindow::saveSettings() const {
    QSettings settings("uniud.it", "scribbling_client");

    settings.beginGroup("Aircraft");
    settings.setValue("flight_mode", flightMode);
    settings.setValue("initial_height", QString::number(initialHeight));
    settings.endGroup();
    
    settings.beginGroup("Server");
    settings.setValue("ip_addr", ip_addr);
    settings.setValue("tcp_port", tcp_port);
    settings.setValue("scale_factor", scale_factor);
    settings.endGroup();
    
    settings.beginGroup("Client");
    settings.setValue("sampling_factor", sampling_factor);
    settings.endGroup();
}

// Loads previously saved settings. If a setting is 
// not found or the config file doesn't exist yet,
// default values will be assigned.
void MainWindow::loadSettings() {
    QSettings settings("uniud.it", "scribbling_client");
    
    settings.beginGroup("Aircraft");
    flightMode = settings.value("flight_mode", false).toBool();
    initialHeight = settings.value("initial_height", 1.1).toFloat();
    settings.endGroup();
    
    settings.beginGroup("Server");
    ip_addr = settings.value("ip_addr", "0.0.0.0").toString();
    tcp_port = settings.value("tcp_port", 5000).toInt();
    scale_factor = settings.value("scale_factor", 8).toInt();
    settings.endGroup();
    
    settings.beginGroup("Client");
    sampling_factor = settings.value("sampling_factor", 3).toInt();
    settings.endGroup();
    
    netSettings->setOnboardIPAddress(ip_addr);
    netSettings->setOnboardTcpPort(tcp_port);
    netSettings->setScaleFactor(scale_factor);
    scribbleArea->setScaleFactor(scale_factor);
    netSettings->setSamplingFactor(sampling_factor);
    scribbleArea->setCursorSamplingFactor(sampling_factor);
    
    aircftSettings->setFlightMode(flightMode);
    scribbleArea->setFlightMode(flightMode);
    aircftSettings->setInitialHeight(initialHeight);
    scribbleArea->setInitialHeight(initialHeight);
    updateFlightModeLabel();
}