/*
 * aircraft_settings.cpp
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


#include "aircraft_settings.h"

AircraftSettings::AircraftSettings() {    
    createFlightModeSettings();    
    createInitialHeightSettings();
   
    confirmButtonBox = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
    connect(confirmButtonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(confirmButtonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    
    QVBoxLayout *vlayout = new QVBoxLayout;  
  
    vlayout->addWidget(flightModeSettingsGroup);
    vlayout->addWidget(initialHeightSettingsGroup);
    vlayout->addWidget(confirmButtonBox);
    setLayout(vlayout);
    setWindowTitle(tr("Aircraft Settings"));
}

void AircraftSettings::createFlightModeSettings() {
    horiFlightModeRadioButton = new QRadioButton("Horizontal   [move only on x-y plane]", this);
    vertFlightModeRadioButton = new QRadioButton("Vertical       [move only on y-z plane]", this);
    
    horiFlightModeRadioButton->setChecked(true);
    vertFlightModeRadioButton->setChecked(false);
    
    flightModeSettingsGroup = new QGroupBox(tr("Flight mode"));
    QFormLayout *layout = new QFormLayout;
    layout->addRow(horiFlightModeRadioButton);
    layout->addRow(vertFlightModeRadioButton);
    flightModeSettingsGroup->setLayout(layout);
}

void AircraftSettings::createInitialHeightSettings() {
    initialHeightLabel = new QLabel(tr("Desired height [m]:"));
  
    initialHeightSpinBox = new QDoubleSpinBox;
    initialHeightSpinBox->setMinimum(1.1);
    initialHeightSpinBox->setMaximum(100);
    initialHeightSpinBox->setSingleStep(0.1);
    
    initialHeightSettingsGroup = new QGroupBox(tr("Initial height after take-off"));
    QFormLayout *layout = new QFormLayout;
    layout->addRow(initialHeightLabel, initialHeightSpinBox);    
    initialHeightSettingsGroup->setLayout(layout);
}

bool AircraftSettings::getFlightMode() const {
    if(vertFlightModeRadioButton->isChecked())
        return true;
    else return false;
}

void AircraftSettings::setFlightMode(const bool &flightMode) {
    if(flightMode) {
         horiFlightModeRadioButton->setChecked(false);
         vertFlightModeRadioButton->setChecked(true);
    }
    else {
        horiFlightModeRadioButton->setChecked(true);
        vertFlightModeRadioButton->setChecked(false);
    }
}

void AircraftSettings::setInitialHeight(const float &initialHeight) {
    initialHeightSpinBox->setValue(initialHeight); 
}