/*
 * scribblearea.h
 * 
 * This is a modified version of the ScribbleArea class
 * (https://doc.qt.io/qt-5/qtwidgets-widgets-scribble-example.html)
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef SCRIBBLEAREA_H
#define SCRIBBLEAREA_H

#include <utility> // needed in order to use the pair structure
#include <limits>  // numeric limits (see mouseMoveEventCounter attr)

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>

#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonDocument>

#include <QToolTip>
#include <QDebug>
#include <QMessageBox>

using namespace std;


class ScribbleArea : public QWidget
{
    Q_OBJECT

public:
    ScribbleArea(QWidget *parent = nullptr);

    bool saveImage(const QString &fileName, const char *fileFormat);
    void setPenColor(const QColor &newColor);
    void setPenWidth(int newWidth);

    bool isModified() const { return modified; }
    QColor penColor() const { return myPenColor; }
    int penWidth() const { return myPenWidth; }

    pair<int,int> getLastPoint() const;
    
    void setCursorSamplingFactor(const int &cursorSamplingFactor);
    void setScaleFactor(const int &scaleFactor);
    void setFlightMode(const bool &flightMode);
    void setInitialHeight(const float &initialHeight);
    void startTcpConn(const QString &ip_addr, const int &tcp_port);
    void stopTcpConn();
    int getConnState() const { return tcp_socket.state(); }
    void sendTakeOffCmd();
    void sendLandingCmd();
    void sendMousePressEvent();
    void sendMouseReleaseEvent();
    void sendTcpData();
    
public slots:
    void clearImage();
    void print();
    void readTcpData();
    void completeHandshake();
    void cleanup();
    
signals:
    void onboardStatusChanged(const int &statusCode);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    void drawLineTo(const QPoint &endPoint);
    void resizeImage(QImage *image, const QSize &newSize);
    bool pointIsValid(const int &x, const int &y) const;

    bool modified = false;
    bool scribbling = false;
    bool flightMode = false;
    float initialHeight = 1.1;
    int onboardStatusCode = 2;
    int cursorSamplingFactor = 1;
    int scaleFactor = 1;
    int myPenWidth = 3;
    int mouseMoveEventCounter = 0;
    QColor myPenColor = Qt::blue;
    QImage image;
    QPoint lastPoint;
    QTcpSocket tcp_socket;
    QJsonObject data;
};

#endif
