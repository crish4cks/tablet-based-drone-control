/*
 * scribblearea.cpp
 * 
 * This is a modified version of the ScribbleArea class
 * (https://doc.qt.io/qt-5/qtwidgets-widgets-scribble-example.html)
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 */


/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#include <iostream> 
#include <cmath>

#include "scribblearea.h"

#include <QMouseEvent>
#include <QPainter>

#if defined(QT_PRINTSUPPORT_LIB)
#include <QtPrintSupport/qtprintsupportglobal.h>
#if QT_CONFIG(printdialog)
#include <QPrinter>
#include <QPrintDialog>
#endif
#endif

#include <QString>
#include <QHostAddress>


ScribbleArea::ScribbleArea(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_StaticContents);

    setMouseTracking(true);

    connect(&tcp_socket, SIGNAL(readyRead()), this, SLOT(readTcpData()));
    connect(&tcp_socket, SIGNAL(connected()), this, SLOT(completeHandshake()));
    connect(&tcp_socket, SIGNAL(disconnected()), this, SLOT(cleanup()));
}

bool ScribbleArea::saveImage(const QString &fileName, const char *fileFormat)
{
    QImage visibleImage = image;
    resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
        modified = false;
        return true;
    }
    return false;
}

void ScribbleArea::setPenColor(const QColor &newColor)
{
    myPenColor = newColor;
}

void ScribbleArea::setPenWidth(int newWidth)
{
    myPenWidth = newWidth;
}

void ScribbleArea::clearImage()
{
    image.fill(qRgb(255, 255, 255));
    modified = true;
    update();
}

// Returns the coordinates in pixels of the current position of the cursor
// with respect to the drawing area
pair<int,int> ScribbleArea::getLastPoint() const {
    pair<int,int> lastPoint;
    
    int x = this->lastPoint.x();
    int y = this->lastPoint.y();
    lastPoint = make_pair(x,y);    
    
    return lastPoint;
}

void ScribbleArea::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton && (onboardStatusCode == 1)) {
        lastPoint = event->pos();
        scribbling = true;
        sendMousePressEvent();
    }
}

void ScribbleArea::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton && scribbling && (onboardStatusCode == 1)) {
        drawLineTo(event->pos());
        scribbling = false;
	sendMouseReleaseEvent();
    }
}   

void ScribbleArea::mouseMoveEvent(QMouseEvent *event) {
    // Display mouse coordinates as tooltip
    if(flightMode) {    // y-z plane
        QToolTip::showText(event->globalPos(), 
                           QString::number(event->pos().x()) + ", " +
                           QString::number(this->size().height() - event->pos().y() + initialHeight * scaleFactor),
                           this, rect());      
    }
    else {              // x-y plane     
        QToolTip::showText(event->globalPos(), 
                           QString::number(event->pos().x()) + ", " +
                           QString::number(event->pos().y()),
                           this, rect());
    }

    if ((event->buttons() & Qt::LeftButton) && scribbling && (onboardStatusCode == 1)) {
        drawLineTo(event->pos()); 

        // Sample every 'cursorSamplingFactor' points and send data
        if((mouseMoveEventCounter % cursorSamplingFactor) == 0)
           sendTcpData();

        // Reset counter if the maximum value is reached
        if(mouseMoveEventCounter < numeric_limits<int>::max())
            mouseMoveEventCounter++;
        else mouseMoveEventCounter = 0;
    }
}

// Sets the desired cursor sampling factor
void ScribbleArea::setCursorSamplingFactor(const int &cursorSamplingFactor) {
   this->cursorSamplingFactor = cursorSamplingFactor;    
}

// Sets the scale factor for the aircraft coordinates conversion
void ScribbleArea::setScaleFactor(const int &scaleFactor) {
    this->scaleFactor = scaleFactor;
}

// Sets the flight mode for the aircraft
void ScribbleArea::setFlightMode(const bool &flightMode) {
    this->flightMode = flightMode;  
}

// Sets the initial height after take off
void ScribbleArea::setInitialHeight(const float &initialHeight) {
    this->initialHeight = initialHeight;  
}

// Starts a TCP connection with the onboard computer
void ScribbleArea::startTcpConn(const QString &ip_addr, const int &tcp_port) {    
    tcp_socket.connectToHost(QHostAddress(ip_addr), tcp_port);
}

// Closes a previously opened TCP connection
void ScribbleArea::stopTcpConn() {
    if(tcp_socket.state() == QTcpSocket::ConnectedState)
        tcp_socket.disconnectFromHost();
}

// Sends a TAKE_OFF cmd to the onboard computer
void ScribbleArea::sendTakeOffCmd() {
    if(tcp_socket.state() == QTcpSocket::ConnectedState) {
        data.insert(QString("msgType"), QJsonValue("TAKE_OFF"));
        sendTcpData();
    } 
}

// Sends a LANDING cmd to the onboard computer
void ScribbleArea::sendLandingCmd() {
    if(tcp_socket.state() == QTcpSocket::ConnectedState) {
        data.insert(QString("msgType"), QJsonValue("LANDING"));
        sendTcpData();
    }  
}

// Signals to the onboard computer that the left mouse button has been pressed
void ScribbleArea::sendMousePressEvent() {
    if(tcp_socket.state() == QTcpSocket::ConnectedState) {
        data.insert(QString("msgType"), QJsonValue("MOUSE_EVENT"));
        data.insert(QString("mouseLeftButton"), QJsonValue("PRSD"));
        sendTcpData();
    }
}

// Signals to the onboard computer that the left mouse button has been released
void ScribbleArea::sendMouseReleaseEvent() {
    if(tcp_socket.state() == QTcpSocket::ConnectedState) {
        data.insert(QString("msgType"), QJsonValue("MOUSE_EVENT"));
        data.insert(QString("mouseLeftButton"), QJsonValue("RLSD"));
        sendTcpData();
    }
}

// Sends data in compat JSON format over a previously established TCP connection
//
// Example of payload: 
// {"flightMode":"VERTICAL","msgType":"POSITION","scaleFactor":100,"x":0,"y":212,"z":734}
//
void ScribbleArea::sendTcpData() {
    int x, y, z;
    
    if(flightMode) {    // y-z plane
	if(data.contains(QString("msgType")) && data.value(QString("msgType")).toString() == QString("TAKE_OFF")) {
	    // Reset coordinates
	    x = 0;
            y = 0;
            z = initialHeight * scaleFactor;  
	}
        else {
            x = 0;
            y = this->getLastPoint().first;
            z = this->size().height() - this->getLastPoint().second + initialHeight * scaleFactor;
	}
	data.insert(QString("flightMode"), QJsonValue("VERTICAL"));
    }
    else {              // x-y plane
        if(data.contains(QString("msgType")) && data.value(QString("msgType")).toString() == QString("TAKE_OFF")) {
	    // Reset coordinates
            x = 0;
            y = 0;
            z = initialHeight * scaleFactor;
	}
        else {
            x = this->getLastPoint().first;
            y = this->getLastPoint().second;
            z = initialHeight * scaleFactor;
	}
	data.insert(QString("flightMode"), QJsonValue("HORIZONTAL"));
    }
    
    // Check if the msgType param has already been set (i.e. MOUSE_EVENT, TAKE_OFF, LANDING or HANDSHAKE)
    // If not, it means thate we have to set it with the POSITION value 
    if(!data.contains(QString("msgType")))
       data.insert(QString("msgType"), QJsonValue("POSITION"));
    
    data.insert(QString("x"), QJsonValue(x));
    data.insert(QString("y"), QJsonValue(y));
    data.insert(QString("z"), QJsonValue(z));
    data.insert(QString("scaleFactor"), QJsonValue(scaleFactor));
    
    QJsonDocument doc(data);
    
    if(flightMode) {    // y-z plane
        if((tcp_socket.state() == QTcpSocket::ConnectedState) && pointIsValid(y, z))
            tcp_socket.write(doc.toJson(QJsonDocument::Compact));
    }
    else {              // x-y plane
        if((tcp_socket.state() == QTcpSocket::ConnectedState) && pointIsValid(x, y))
            tcp_socket.write(doc.toJson(QJsonDocument::Compact));
    }

    data = QJsonObject();  // Clear the object once the data has been sent
}

// Reads the response from the onboard computer and emits a signal
// according to the onboard computer status
void ScribbleArea::readTcpData() {
    QByteArray response;
    response = tcp_socket.readAll();
    
    QJsonDocument doc = QJsonDocument::fromJson(response);
    QJsonObject responseObject = doc.object();
    
    QString onboardStatus = responseObject.value("status").toString();
    int errorCode = 0;
    
    if(onboardStatus == QString("INIT")) {
        onboardStatusCode = 0;
        emit onboardStatusChanged(onboardStatusCode);  
    }
    else if(onboardStatus == QString("READY")) {
        onboardStatusCode = 1;
        emit onboardStatusChanged(onboardStatusCode);
    }
    else if(onboardStatus == QString("BUSY")) {
        onboardStatusCode = 2;
        emit onboardStatusChanged(onboardStatusCode);   
    }
    else {    // {"status":"ERROR"}
        onboardStatusCode = 3;
        emit onboardStatusChanged(onboardStatusCode);
	// Retrieve error code from JSON
        errorCode = responseObject.value("errorCode").toInt();
	// Retrieve error message from JSON
	QString errorMsg = responseObject.value("errorMsg").toString();
        QMessageBox errMsgBox;
        errMsgBox.setWindowTitle("Fatal error!");
        errMsgBox.setIcon(QMessageBox::Critical);
        errMsgBox.setText(QString("Please, switch to manual controls\n[errorCode = %1, errorMsg: %2]").arg(errorCode).arg(errorMsg));
        errMsgBox.exec();
    }
    
    // Print onboard TCP server response on cmd line
    cout << response.toStdString() << endl;
}


// Enables KeepAlive option and completes the handshake with the server
void ScribbleArea::completeHandshake() {
    // Enable KeepAlive option
    tcp_socket.setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    
    // Complete the handshake
    data.insert(QString("msgType"), QJsonValue("HANDSHAKE"));
    sendTcpData();
}

// Checks if a point is out of the widget area (in terms of coordinates)
bool ScribbleArea::pointIsValid(const int &x, const int &y) const {
    int max_x = this->size().width();
    int max_y = this->size().height();
    int min_y;
    
    if(flightMode)
        min_y = initialHeight * scaleFactor;
   else min_y = 0;
    
    if(x < 0 || y < min_y || x > max_x || y > (max_y + min_y))
        return false;
    else return true;
}

void ScribbleArea::paintEvent(QPaintEvent *event) 
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, image, dirtyRect);
}

void ScribbleArea::resizeEvent(QResizeEvent *event)
{
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}

void ScribbleArea::drawLineTo(const QPoint &endPoint)
{
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(lastPoint, endPoint);
    modified = true;

    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
}

void ScribbleArea::resizeImage(QImage *image, const QSize &newSize)
{
    if (image->size() == newSize)
        return;

    QImage newImage(newSize, QImage::Format_RGB32);
    newImage.fill(qRgb(255, 255, 255));
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}

void ScribbleArea::print()
{
#if QT_CONFIG(printdialog)
    QPrinter printer(QPrinter::HighResolution);

    QPrintDialog printDialog(&printer, this);

    if (printDialog.exec() == QDialog::Accepted) {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = image.size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(image.rect());
        painter.drawImage(0, 0, image);
    }
#endif // QT_CONFIG(printdialog)
}

// Cleanup once we are no more connected
void ScribbleArea::cleanup() {
    scribbling = false;
    onboardStatusCode = 2;  
}