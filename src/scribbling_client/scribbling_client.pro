QT += network
QT += core
QT += widgets
requires(qtConfig(filedialog))
qtHaveModule(printsupport): QT += printsupport

HEADERS = mainwindow.h \
          scribblearea.h \
          network_settings.h \
          aircraft_settings.h
SOURCES = main.cpp \
          mainwindow.cpp \
          scribblearea.cpp \
          network_settings.cpp \
          aircraft_settings.cpp
