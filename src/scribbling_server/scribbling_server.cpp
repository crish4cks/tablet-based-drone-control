/*
 * Scribbling Server version 0.1
 * 
 * Author: Cristiano Urban (https://crish4cks.net)
 * Mail: cristiano[dot]urban[dot]slack[at]gmail[dot]com,
 *       urban[dot]cristiano[dot]1[at]spes[dot]uniud[dot]it
 * 
 * Usage: ./scribbling_server <config_file.txt> <ip_address> <tcp_port>
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <time.h>


#ifndef DUMMY_SERVER
    extern "C" {
        #include "tcpsocketlib.h"
        #include "jsmn.h"
    }

    #include "flight_control_sample.hpp"
    #include "flight_sample.hpp"
  
    using namespace DJI::OSDK;
    using namespace DJI::OSDK::Telemetry;

    // Vehicle object pointer
    Vehicle *vehicle;

#else
    #include "tcpsocketlib.h"
    #include "jsmn.h"
    
    // Dummy vehicle
    char vehicle = 'v';
#endif


// Auxiliary function used into the JSON parsing section.
// For more information refer to https://github.com/zserge/jsmn
static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
    if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
        strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
        return 0;
    }
    return -1;
}

// Struct to save the point coordinates
struct Point {
    float x;  
    float y;
    float z;
};

// Data received from the client + additional data
struct clientData {
    char *msgType;
    char *mouseLeftButton;
    float scaleFactor;
    int takeOffFlag;
    char *flightMode;
    struct Point currentCoords;
    struct Point previousCoords;
} dataRcvd;

// Preprocessor trick to print enum values as strings
#define ONBOARD_STATES S(INIT)S(READY)S(BUSY)S(ERROR)
#define S(x) x,
enum stateNum { ONBOARD_STATES N };
#undef S
#define S(x) #x,
const char * const state[] = { ONBOARD_STATES };

#define ERROR_STATES E(NONE)E(JSON_PARSE_ERR)E(TAKE_OFF_ERR)E(MOVE_ERR)E(LANDING_ERR)
#define E(x) x,
enum error { ERROR_STATES M };
#undef E
#define E(x) #x,
const char * const errorState[] = { ERROR_STATES };

// Onboard computer state variable
int onboardState = INIT;

// Error state variable
int errorCode = NONE;

// Functions prototypes
#ifdef DUMMY_SERVER
    int monitoredTakeoff(char vehicle);
    int monitoredLanding(char vehicle);
    int moveByPositionOffset(char vehicle, float x, float y, float z, float yaw);
#endif
int parseJsonObject(char *payload);
void sendErrorMsg(int csk, int error);
void logger(const char *msg, int success);


// Main function
int main(int argc, char *argv[]) {  
    // Set previous point coordinates to zero
    dataRcvd.previousCoords.x = 0.0;
    dataRcvd.previousCoords.y = 0.0;
    dataRcvd.previousCoords.z = 0.0;
    
    #ifdef DUMMY_SERVER
        #ifdef SIM_MANUAL_TAKEOFF
            dataRcvd.previousCoords.z = 400.0;
        #endif
        #ifdef SIM_MANUAL_LANDING
            onboardState = READY;
        #endif
    #endif
    
    // Set the take off flag to zero
    dataRcvd.takeOffFlag = 0;

    // Check the number of cli input args
    if (argc != 4) {
        fprintf (stderr, "Required arguments: [dji_user_config.txt] [manifold_ip_address] [tcp_port]\n");
        exit(EXIT_FAILURE);
    }
    
    // Remove old log file, if any
    if(access("scribbling_server.log", F_OK ) != -1)
        remove("scribbling_server.log");
    
    #ifndef DUMMY_SERVER
        // Setup OSDK
        LinuxSetup linuxEnvironment(argc, argv);
        vehicle = linuxEnvironment.getVehicle();
        if(vehicle == NULL) {
            printf("Vehicle not initialized, exiting.\n");
            exit(EXIT_FAILURE);
        }
    #endif
    
    // Start the TCP server
    printf("\n============================== Scribbling Server ==============================\n");  
    logger("Starting TCP server...", 1);
    if(create_tcp_server(argv[2], atoi (argv[3])) < 0) {
        logger("Error creating TCP server", 0);
        exit(EXIT_FAILURE);
    }
    
    return EXIT_SUCCESS;
}


int server_handler (int csk, char *ip_addr, int port) {
    char buffer[BUFSIZ + 1];
    char payload[256], cmdRequest[256], logData[256];
    float x_offset, y_offset, z_offset;
    
    #ifndef DUMMY_SERVER
        // Obtain Control Authority with a function timeout of 1 s
        vehicle->obtainCtrlAuthority(1);
        sleep(1);
        // Needed to know the current height when the aircraft is in air
        Telemetry::GlobalPosition currentBroadcastGP;
    #endif    
    
    logger("New incoming connection!", 1);
    sprintf(logData, "Connected to %s:%d", ip_addr, port);
    logger(logData, 1);
    
    // Complete the connection handshake with the Scribbling Client
    if(tcp_receive(csk, buffer) > 0) {                          // Success
        logger("Received new payload!", 1);
	sscanf(buffer, "%s", payload);
	logger(payload, 1);
        // Parse the JSON Object received from the Scribbling Client
	logger("Parsing JSON object...", 1);
	if(!parseJsonObject(payload)) {
	   logger("Cannot parse JSON object", 0);
	   sendErrorMsg(csk, JSON_PARSE_ERR);
	}
	logger("JSON object parsed successfully!", 1);
    } else logger("Cannot complete the initial handshake", 0);  // Failure
    
    // Send back the current status to the Scribbling Client
    if(onboardState == ERROR) {
        sprintf(logData, "Current server status: %s", state[onboardState]);
        logger(logData, 0);
        sendErrorMsg(csk, errorCode);  
    }
    else {
        #ifndef DUMMY_SERVER
            // If the aircraft is currently in air and the current state is INIT, it means that a manual take off has occured
            if(vehicle->broadcast->getStatus().flight >= DJI::OSDK::VehicleStatus::M100FlightStatus::TAKEOFF && onboardState == INIT) {
                // Get the height value immediately after take off and store it in
                // dataRcvd.previousCoords.z
                currentBroadcastGP = vehicle->broadcast->getGlobalPosition();
                sleep(1);
                dataRcvd.previousCoords.z = currentBroadcastGP.height * dataRcvd.scaleFactor;
                onboardState = READY;
                logger("Manual TAKE OFF detected. Server status changed to READY", 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
		else logger("Cannot send server status", 0);
            }
            // If the aircraft is currently on ground and the current state is READY, it means that a manual landing has occured
            else if(vehicle->broadcast->getStatus().flight < DJI::OSDK::VehicleStatus::M100FlightStatus::TAKEOFF && onboardState == READY) {
	        sleep(1);
                // Reset all the coordinates
                dataRcvd.previousCoords.x = 0.0;
                dataRcvd.previousCoords.y = 0.0;
                dataRcvd.previousCoords.z = 0.0;
	        onboardState = INIT;
                logger("Manual LANDING detected. Server status changed to INIT", 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
		else logger("Cannot send server status", 0);  
	    }
            else {
	        sprintf(logData, "Current server status: %s", state[onboardState]);
	        logger(logData, 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
                else logger("Cannot send server status", 0);
	    }
        #else
            // Simulate a manual take off: dataRcvd.previousCoords.z is initialized to 400 px 
            // corresponding to 4.0 m, with a scaleFactor of 100 px/m.
            // In order to simulate a manual take off you have to build the Scribbling Server
            // with 'make mtakeoff'
            if(dataRcvd.previousCoords.z > 0.3 && onboardState == INIT) {
                onboardState = READY;
                logger("Manual TAKE OFF detected. Server status changed to READY", 1);
                sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
                logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
                else logger("Cannot send server status", 0);  
	    }
	    // Simulate a manual landing: dataRcvd.previousCoords.z is initialized to 0 px
	    // corresponding to 0 m (ground), with any scaleFactor.
	    // In order to simulate a manual landing you have to build the Scribbling Server
            // with 'make mlanding'
	    else if(dataRcvd.previousCoords.z < 0.1 && onboardState == READY) {
	        onboardState = INIT;
                logger("Manual LANDING detected. Server status changed to INIT", 1);
                sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
                logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
                else logger("Cannot send server status", 0);
	    }
	    else {
                sprintf(logData, "Current server status: %s", state[onboardState]);
	        logger(logData, 1);
                sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
	        logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
                else logger("Cannot send server status", 0);
	    }
        #endif
    }
    
    // Wait for the Scribbling Client cmd
    while(tcp_receive(csk, buffer) > 0) {
	logger("Received new payload!", 1);
	sscanf(buffer, "%s", payload);
	logger(payload, 1);

	// Parse the JSON Object received from the Scribbling Client
	logger("Parsing JSON object...", 1);
	if(!parseJsonObject(payload)) {
	   logger("Cannot parse JSON object", 0);
	   sendErrorMsg(csk, JSON_PARSE_ERR);
	}
	logger("JSON object parsed successfully!", 1);

	// Coordinate transformation from pixels to meters 
	if(strcmp(dataRcvd.flightMode, "VERTICAL") == 0) {    // y-z plane
            x_offset = 0.0;
            y_offset = (dataRcvd.currentCoords.y - dataRcvd.previousCoords.y) / dataRcvd.scaleFactor;
            z_offset = (dataRcvd.currentCoords.z - dataRcvd.previousCoords.z) / dataRcvd.scaleFactor;
	}  
	else {                                                // x-y plane
	    x_offset = (-dataRcvd.currentCoords.y + dataRcvd.previousCoords.y) / dataRcvd.scaleFactor;
	    y_offset = (dataRcvd.currentCoords.x - dataRcvd.previousCoords.x) / dataRcvd.scaleFactor;
	    z_offset = 0.0;
	}
	    
        if(onboardState == READY) {
            if(strcmp(dataRcvd.msgType, "POSITION") == 0) {
                onboardState = BUSY;
		logger("Server status changed to BUSY", 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
		else logger("Cannot send server status", 0);
		sprintf(logData, "Starting MOVE action (POSITION): x_offset = %.3f, y_offset = %.3f, z_offset = %.3f", x_offset, y_offset, z_offset);
		logger(logData, 1);
                if(moveByPositionOffset(vehicle, x_offset, y_offset, z_offset, 0)) {  // Error
		    logger("MOVE action cannot be done", 0);
		    sendErrorMsg(csk, MOVE_ERR);
		}
		else {                                                                // Success
	            dataRcvd.previousCoords = dataRcvd.currentCoords;
		    onboardState = READY;
		    logger("MOVE action done successfully!", 1);
		    logger("Server status changed to READY", 1);
	            sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		    logger("Sending server status to Scribbling Client...", 1);
		    if(tcp_send(csk, cmdRequest))
		        logger("Server status sent successfully!", 1);
		    else logger("Cannot send server status", 0);
		}
            }
            else if(strcmp(dataRcvd.msgType, "MOUSE_EVENT") == 0) {
	        onboardState = BUSY;
		logger("Server status changed to BUSY", 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
                    logger("Server status sent successfully!", 1);
		else logger("Cannot send server status", 0);
	        if(strcmp(dataRcvd.mouseLeftButton, "PRSD") == 0) {
		    sprintf(logData, "Starting MOVE action (MOUSE_EVENT_PRESSED): x_offset = %.3f, y_offset = %.3f, z_offset = %.3f", x_offset, y_offset, z_offset);
		    logger(logData, 1);
		    if(moveByPositionOffset(vehicle, x_offset, y_offset, z_offset, 0)) {  // Error
		        logger("MOVE action cannot be done", 0);
		        sendErrorMsg(csk, MOVE_ERR);
		    }
		    else {                                                                // Success
	                dataRcvd.previousCoords = dataRcvd.currentCoords;
		        onboardState = READY;
			logger("MOVE action done successfully!", 1);
		        logger("Server status changed to READY", 1);
	                sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
			logger("Sending server status to Scribbling Client...", 1);
		        if(tcp_send(csk, cmdRequest))
                            logger("Server status sent successfully!", 1);
		        else logger("Cannot send server status", 0);
			
                        ///////////////////////////////////////////
                        /// Place the code of your action here! ///
                        ///////////////////////////////////////////
			
		    }
		}
		else if(strcmp(dataRcvd.mouseLeftButton, "RLSD") == 0) {
		    sprintf(logData, "Starting MOVE action (MOUSE_EVENT_RELEASED): x_offset = %.3f, y_offset = %.3f, z_offset = %.3f", x_offset, y_offset, z_offset);
		    logger(logData, 1);
		    if(moveByPositionOffset(vehicle, x_offset, y_offset, z_offset, 0)) {  // Error
		        logger("MOVE action cannot be done", 0);
                        sendErrorMsg(csk, MOVE_ERR);
		    }
		    else {                                                                // Success
	                dataRcvd.previousCoords = dataRcvd.currentCoords;
		        onboardState = READY;
			logger("MOVE action done successfully!", 1);
		        logger("Server status changed to READY", 1);
	                sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
			logger("Sending server status to Scribbling Client...", 1);
		        if(tcp_send(csk, cmdRequest))
                            logger("Server status sent successfully!", 1);
		        else logger("Cannot send server status", 0);
			
                        ///////////////////////////////////////////
                        /// Place the code of your action here! ///
                        ///////////////////////////////////////////
			
		    }
		}
		else {
		    // Unknown mouse event: skip  
		    logger("Unknown mouse event: skip", 1);
		}
	    }
	    else if(strcmp(dataRcvd.msgType, "LANDING") == 0) {
	        onboardState = BUSY;
		logger("Server status changed to BUSY", 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
                if(tcp_send(csk, cmdRequest))
		    logger("Server status sent successfully!", 1);
		else logger("Cannot send server status", 0);
		logger("Starting LANDING...", 1);
	        if(!monitoredLanding(vehicle)) {  // Error
		    logger("Cannot start LANDING", 0);
                    sendErrorMsg(csk, LANDING_ERR);
		}
		else {                            // Success
		    // Reset all the coordinates
		    dataRcvd.previousCoords.x = 0.0;
                    dataRcvd.previousCoords.y = 0.0;
                    dataRcvd.previousCoords.z = 0.0;
		    onboardState = INIT;
		    logger("LANDING done successfully!", 1);
		    logger("Server status changed to INIT", 1);
	            sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		    logger("Sending server status to Scribbling Client...", 1);
		    if(tcp_send(csk, cmdRequest))
		        logger("Server status sent successfully!", 1);
		    else logger("Cannot send server status", 0);
		}
	    }
	    else {          
	        // Unknown command: skip
	        logger("Unknown command: skip", 1);
	    }
	}
	else if(onboardState == INIT) { 
	    if(strcmp(dataRcvd.msgType, "TAKE_OFF") == 0) {
	        onboardState = BUSY;
		logger("Server status changed to BUSY", 1);
	        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		logger("Sending server status to Scribbling Client...", 1);
		if(tcp_send(csk, cmdRequest))
		    logger("Server status sent successfully!", 1);
		else logger("Cannot send server status", 0); 
		sleep(3);    // needed to avoid simulation error L752 with DJI Assistant 2 after landing
		logger("Starting TAKE OFF...", 1);
	        if(!monitoredTakeoff(vehicle)) {  // Error
		    logger("Cannot start TAKE OFF", 0);
                    sendErrorMsg(csk, TAKE_OFF_ERR);
		}
		else {                            // Success
		    // Get the height value immediately after take off and store it in 
                    // dataRcvd.previousCoords.z
                    #ifndef DUMMY_SERVER
                        currentBroadcastGP = vehicle->broadcast->getGlobalPosition();
			sleep(1);
                        dataRcvd.previousCoords.z = currentBroadcastGP.height * dataRcvd.scaleFactor;
                    #else
			sleep(1);
                        dataRcvd.previousCoords.z = 110;
                    #endif
		    logger("TAKE OFF done successfully!", 1);
                    dataRcvd.takeOffFlag = 1;
		    if(dataRcvd.currentCoords.z > dataRcvd.previousCoords.z) {
		        z_offset = (dataRcvd.currentCoords.z - dataRcvd.previousCoords.z) / dataRcvd.scaleFactor;
		        sprintf(logData, "Starting MOVE action (INITIAL_HEIGHT): x_offset = %.3f, y_offset = %.3f, z_offset = %.3f", x_offset, y_offset, z_offset);
		        logger(logData, 1);
                        if(moveByPositionOffset(vehicle, 0, 0, z_offset, 0)) {  // Error
		            logger("MOVE action cannot be done", 0);
		            sendErrorMsg(csk, MOVE_ERR);
			    dataRcvd.takeOffFlag = 0;
		        }
		        else {                                                  // Success 
                            logger("MOVE action done successfully!", 1);
                            // Get the new height value immediately after move action and store it in 
                            // dataRcvd.previousCoords.z
                            #ifndef DUMMY_SERVER
                                currentBroadcastGP = vehicle->broadcast->getGlobalPosition();
                                sleep(1);
                                dataRcvd.previousCoords.z = currentBroadcastGP.height * dataRcvd.scaleFactor;
                            #else
			        sleep(1);
                                //dataRcvd.previousCoords.z = 400;
				dataRcvd.previousCoords.z = dataRcvd.currentCoords.z;
                           #endif
                           dataRcvd.takeOffFlag = 1;
			}
		    }
		    if(dataRcvd.takeOffFlag) {
		        onboardState = READY;
		        logger("Server status changed to READY", 1);
                        sprintf(cmdRequest, "{\"status\":\"%s\"}", state[onboardState]);
		        logger("Sending server status to Scribbling Client...", 1);
                        if(tcp_send(csk, cmdRequest))
		            logger("Server status sent successfully!", 1);
		        else logger("Cannot send server status", 0);
		    }
		}
	    }
            else {
                // Unknown command: skip
	        logger("Unknown command: skip", 1);
            }
        }
	else if(onboardState == ERROR) {
	    logger("Server is blocked on ERROR status", 0);
	    sendErrorMsg(csk, errorCode);
	}
	else {
	    // BUSY: do nothing until the state changes to READY or INIT
	    logger("Server is BUSY, wait until the state changes to READY or INIT...", 1);
	}
    }
    
    #ifndef DUMMY_SERVER
        // Release Control Authority with a function timeout of 1 s
        vehicle->releaseCtrlAuthority(1);
    #endif

    sprintf(logData, "%s:%d has just disconnected", ip_addr, port);
    logger(logData, 1);
    
    // Keep the TCP server alive
    return 1;
}

int parseJsonObject(char *payload) {
    int i, r;
    jsmn_parser p;
    jsmntok_t t[32];
    
    jsmn_init(&p);
	
    // Returns an integer representing the number of tokens of the JSON object
    r = jsmn_parse(&p, payload, strlen(payload), t, sizeof(t) / sizeof(t[0]));
	    
    for(i = 1; i < r; i++) {
        if(jsoneq(payload, &t[i], "msgType") == 0) {
	    dataRcvd.msgType = strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start);
	    i++;
	}
	else if(jsoneq(payload, &t[i], "flightMode") == 0) {
            dataRcvd.flightMode = strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start);
            i++;
        }
        else if(jsoneq(payload, &t[i], "mouseLeftButton") == 0) {
            dataRcvd.mouseLeftButton = strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start);
            i++;
        } 
        else if(jsoneq(payload, &t[i], "scaleFactor") == 0) {
            dataRcvd.scaleFactor = atof(strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start));
            i++;
        }
        else if(jsoneq(payload, &t[i], "x") == 0) {
            dataRcvd.currentCoords.x = atof(strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start));
            i++;  
        }
        else if(jsoneq(payload, &t[i], "y") == 0) {
            dataRcvd.currentCoords.y = atof(strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start));
            i++;
        }
        else if(jsoneq(payload, &t[i], "z") == 0) {
            dataRcvd.currentCoords.z = atof(strndup(payload + t[i+1].start, t[i+1].end - t[i+1].start));
            i++;
        }
        else {
	    // Error
            return 0;
        }
    }
    return 1;
}

#ifdef DUMMY_SERVER
    // Dummy function to simulate a landing
    int monitoredLanding(char vehicle) {
        sleep(3); // 3s delay 
        return 1;
    }

    // Dummy function to simulate a take off
    int monitoredTakeoff(char vehicle) {
        sleep(3);  
        return 1;
    }

    // Dummy function to simulate a move action
    int moveByPositionOffset(char vehicle, float x, float y, float z, float yaw) {
        usleep(200000);  // 200ms delay
        return 0;
    }
#endif

// Provides logging functionalities
void logger(const char *msg, int success) {
    time_t now;
    int hh, mm, ss, day, month, year;
    FILE *fp;
    char logfile[64];
  
    // Obtain the current time and date
    time(&now);
  
    // Convert to local time format
    struct tm *local = localtime(&now);
  
    // hh:mm:ss
    hh = local->tm_hour;
    mm = local->tm_min;
    ss =local->tm_sec;
      
    // dd/mm/yyyy
    day = local->tm_mday;
    month = local->tm_mon + 1;
    year = local->tm_year + 1900;
    
    sprintf(logfile, "scribbling_server.log");

    if((fp = fopen(logfile, "a")) == NULL)
        error_handler("cannot create log file.");
    
    if(success) {
        printf("[ %02d/%02d/%d %02d:%02d:%02d ]   %s\n", day, month, year, hh, mm, ss, msg);
        fprintf(fp, "[ %02d/%02d/%d %02d:%02d:%02d ]   %s\n", day, month, year, hh, mm, ss, msg);
    }
    else {
        printf("\033[1;31m");
        printf("[ %02d/%02d/%d %02d:%02d:%02d ]   %s  --->  [FAILURE]\n", day, month, year, hh, mm, ss, msg);
        printf("\033[0m"); 
        fprintf(fp, "[ %02d/%02d/%d %02d:%02d:%02d ]   %s  --->  [FAILURE]\n", day, month, year, hh, mm, ss, msg);
    }
    
    fclose(fp);
}

// Sends back an error message to the client
void sendErrorMsg(int csk, int error) {
    char errorStr[256];
    onboardState = ERROR;
    logger("Server status changed to ERROR", 0);
    errorCode = error; 
    sprintf(errorStr, "{\"status\":\"%s\",\"errorCode\":%d,\"errorMsg\":\"%s\"}", state[onboardState], errorCode, errorState[errorCode]);
    logger(errorStr, 0);
    logger("Sending server status and error code to Scribbling Client...", 1);
    if(tcp_send(csk, errorStr))
        logger("Server status and error code sent successfully!", 1);
    else logger("Cannot send server status and error code to Scribbling Client", 0);
}

// Fatal error: kills the program
void error_handler(const char *message) {
    printf("Fatal error: %s\n", message);
    exit(EXIT_FAILURE);
}
